# Start Letter Game app

- clone this repo `git clone git@gitlab.com:Dev.HetfieLD/letters-game.git`
- install dependencies `npm i` or `yarn install`
- start app localy `npm start` or `yarn start`
- open app on [http://localhost:3000](http://localhost:3000)
