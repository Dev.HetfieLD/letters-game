export const createBoard = (arrayBoard: string[]): string[][] => {
  const board: string[][] = [];
  const chunkSize = 4;

  while (arrayBoard.length > 0) {
    const chunk = arrayBoard.splice(0, chunkSize);
    board.push(chunk);
  }

  return board;
};
