import { useEffect, useState } from "react";

export type LetterGameProps = {
  board: string[][];
  words: string[];
};

export const LetterGameBoard = ({ board, words }: LetterGameProps) => {
  const [word, setWord] = useState("");
  const [isValidWord, setIsValidWord] = useState(false);
  const [selectedTiles, setSelectedTiles] = useState<Array<[number, number]>>(
    []
  );
  const [usableLetters, setUsableLetters] = useState<Array<[number, number]>>(
    []
  );

  useEffect(() => {
    const createdWord = word.toLowerCase();
    const isAcceptedWord = words.indexOf(createdWord) > -1 ? true : false;
    setIsValidWord(isAcceptedWord);
  }, [word, words]);

  const getUsableLetters = (tile: [number, number]) => {
    const top = tile[0] - 1 >= 0 && tile[0] - 1 <= 3 ? tile[0] - 1 : undefined;
    const left = tile[1] - 1 >= 0 && tile[1] - 1 <= 3 ? tile[1] - 1 : undefined;
    const right =
      tile[1] + 1 >= 0 && tile[1] + 1 <= 3 ? tile[1] + 1 : undefined;
    const bottom =
      tile[0] + 1 >= 0 && tile[0] + 1 <= 3 ? tile[0] + 1 : undefined;

    const fourLetters = {
      topLetter: top !== undefined ? [top, tile[1]] : undefined,
      leftLetter: left !== undefined ? [tile[0], left] : undefined,
      rightLetter: right !== undefined ? [tile[0], right] : undefined,
      bottomLetter: bottom !== undefined ? [bottom, tile[1]] : undefined,
    };

    return Object.entries(fourLetters).map(([key, value]) => {
      return value;
    });
  };

  const addUsableLetters = (tile: [number, number]) => {
    const allUsableLetters = getUsableLetters(tile);
    const realUsableLetters: Array<[number, number]> = [];
    allUsableLetters.forEach((usableLetter, index) => {
      const isSelected = usableLetter
        ? selectedTiles.some(
            (tile) => tile[0] === usableLetter[0] && tile[1] === usableLetter[1]
          )
        : false;
      const isOnUsableLetters = usableLetter
        ? usableLetters.some(
            (tile) => tile[0] === usableLetter[0] && tile[1] === usableLetter[1]
          )
        : false;

      if (!isSelected && !isOnUsableLetters && usableLetter) {
        realUsableLetters.push(usableLetter as [number, number]);
      }
    });

    setUsableLetters([...realUsableLetters]);
  };

  const clickHandler = (letter: string, tile: [number, number]) => {
    setWord((prevLetters) => prevLetters.concat(letter));
    setSelectedTiles([...selectedTiles, tile]);
    addUsableLetters(tile);
  };

  const clearHandler = () => {
    setWord("");
    setSelectedTiles([]);
    setUsableLetters([]);
  };

  return (
    <div className="game">
      <div className="clear-btn">
        {word.length > 0 && (
          <>
            <div className="clear-btn__text">Clear Word</div>
            <div className="clear-btn__icon" onClick={clearHandler}>
              x
            </div>
          </>
        )}
      </div>

      <div className="board">
        {board.map((row, rowIndex) => {
          return row.map((column, columnIndex) => {
            const isSelected = selectedTiles.some(
              (tile) => tile[0] === rowIndex && tile[1] === columnIndex
            );
            const isUsableLetter =
              usableLetters.length === 0
                ? true
                : usableLetters.some(
                    (tile) => tile[0] === rowIndex && tile[1] === columnIndex
                  );
            return (
              <div
                className={`tile ${isSelected && "selected"}`}
                key={`${columnIndex}-${column}`}
                onClick={() =>
                  !isSelected &&
                  isUsableLetter &&
                  clickHandler(column, [rowIndex, columnIndex])
                }
              >
                {column}
              </div>
            );
          });
        })}
      </div>
      <div className="word">
        <div>{word}</div>

        {word.length > 0 && (
          <div
            className={`word__validation ${isValidWord ? "valid" : "invalid"}`}
          >
            {isValidWord ? "valid" : "invalid"}
          </div>
        )}
      </div>
    </div>
  );
};
