import { useLayoutEffect, useState } from "react";
import { createBoard } from "./utils/createBoard";

import { board } from "./constants/test-board-2.json";
import { words } from "./constants/dictionary.json";

import "./css/app.css";

import { LetterGameBoard } from "./components/game";

const App = () => {
  const [gameBoard, setGameBoard] = useState<string[][]>([]);

  useLayoutEffect(() => {
    const newBoard = createBoard(board);
    setGameBoard([...newBoard]);
  }, []);

  return (
    <div className="App">
      <LetterGameBoard board={gameBoard} words={words} />
    </div>
  );
};

export default App;
